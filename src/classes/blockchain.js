const Block = require("./block");
const getTimestamp = require("../functions/getTimestamp");

//Contendrá la lista de bloques que se creen
class Blockchain {
    //Tendrá como propiedad unica el chain donde estarán las cadenas
    constructor() {
        this.chain = [this.createChainGenesisBlock()];
        this.difficulty = 5 
    }
    //Creamos el primer bloque de la cadena
    createChainGenesisBlock() {
        return new Block(
            getTimestamp(), //Timestamp
            { devCoin: 100 },
            "0" //No hay has previo
        );
    }
    //Obtener el último bloque de la cadena
    getLastBlock() {
        return this.chain[this.chain.length - 1];
    }
    //Agregar bloque a la cadena
    addBlock(newBlock) {
        //Al nuevo bloque agregamos el prev hash que corresponde al último bloque de la blockchain
        newBlock.prevHas = this.getLastBlock().hash;
        //Ahora minamos el bloque
        newBlock.miningBlock(this.difficulty);
        //Agregamos el bloque a la cadena
        this.chain.push(newBlock);
    }

    //Validaciones pertinentes a realizar
    chainValidator() {
        //Recorremos con un for todos los bloques del blockchain para validar si el hash que tiene es valido
        //Empezamos a validar desde el bloque uno, ya que el primer bloque no tendrá hash prev
        for (let chainPosition = 1; chainPosition < this.chain.length; chainPosition++) {
            const currentBlock = this.chain[chainPosition];
            const previusBlock = this.chain[chainPosition - 1];

            //Verificamos que no se hayan presentado cambios en los datos de lo contrario es porque el hash cambio
            if (currentBlock.hash != currentBlock.calculateHash()) {
                //Retornamos false para indicar que algo en la cadena esta mal por lo cual no se debería poder continuar con la transacción
                return false;
            }

            //Verificamos el hash entre el bloque actual y el bloque anterior
            if (currentBlock.prevHas != previusBlock.hash) {
                return false;
            }

            //Si todo bien, es porque todo el blockchain esta OK
        }

        return true;
    }
}

module.exports = Blockchain;
