//Usamos la librería para simular y encriptar la información
const SHA256 = require("crypto-js/sha256")

//Creamos bloque de la blockchain
class Block {
    constructor(timestamp, data, prevHas = "") {
        this.hash = this.calculateHash();
        this.timestamp = timestamp;
        this.data = data;
        this.prevHas = prevHas;
        this.wildcard = 0; //Para recalcular hash y poder minar
    }
    //Función utilizada para generar el HASH
    calculateHash() {
        //Es necesario convertir la data a JSON porque puede llegar en un formato diferente
        return SHA256(this.timestamp + this.prevHas + JSON.stringify(this.data) + (this.wildcard)).toString();
    }

    //Minar bloque, donde el parametro dificultad será la cantidad de ceros necesarios al inicio para poder minar nuestra crypto :P
    miningBlock(difficulty){
        //Mientras no ocurra esto, intentamos seguir encontrando el hash para el bloque
        while(this.hash.substring(0, difficulty) !== Array(difficulty + 1).join('0')){
            this.wildcard++;
            this.hash = this.calculateHash();
        }

        console.log("Bloque minado con el HASH : " + this.hash);

    }
}

module.exports = Block;