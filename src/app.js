// import getTimestamp from "../functions/getTimestamp";
const getTimestamp = require("./functions/getTimestamp");
const Block = require("./classes/block");
const Blockchain = require("./classes/blockchain");

let devCoin = new Blockchain();

console.log("Minando bloque 1");
//Agregamos un primer bloque
devCoin.addBlock(
    new Block(getTimestamp(), {
        devCoin: 4,
    })
);

console.log("Minando bloque 2");
//Agregamos un segundo bloque
devCoin.addBlock(
    new Block(getTimestamp(), {
        devCoin: 6,
    })
);

console.log("Minando bloque 3");
//Agregamos un tercer bloque
devCoin.addBlock(
    new Block(getTimestamp(), {
        devCoin: 5,
    })
);

// console.log("Validando blockchain ", devCoin.chainValidator());

// devCoin.chain[1].data = { devCoin: 50 }; //Tratamos de modificar el bloque 1 de la cadena del blockchain
// devCoin.chain[1].hash = devCoin.chain[1].calculateHash(); // Calculamos el hash, pero al este ya no estar enlazado a uno de la cadena, retorna y diría que la cadena fue corrompida

// console.log("Validando blockchain después de modificar un bloque", devCoin.chainValidator());

//Mostramos la blockchain en la consola
console.log(JSON.stringify(devCoin, null, 2));
